//#include <unistd.h>
#include <iostream>

#include "server.h"

using namespace std;

Server::Server(int port, QObject *parent) :
    port(port),
    QTcpServer(parent) {}

void Server::start() {
    if (this->listen(QHostAddress::Any, port)) {
        createStatusMsg("Server started", StatusMsgType::SUCCESS);
    }
    else {
        createStatusMsg("Server did not start", StatusMsgType::ERROR);
    }
}

void Server::incomingConnection(qintptr handle) {
    Client *client = new Client(handle);
    clients.emplace(handle, client);
    connect(client, SIGNAL(signDeleted(int)), this, SLOT(slotEraseClient(int)));
    connect(client, SIGNAL(signReaded(QString)), this, SLOT(slotReaded(QString)));
    connect(client, SIGNAL(signCreatedStatusMsg(StatusMsg)), this, SLOT(slotCreateStatusMsg(StatusMsg)));
    createStatusMsg("client on port " + to_string(port), StatusMsgType::SUCCESS);
    emit signNewConnection(handle);
}

void Server::slotCreateStatusMsg(StatusMsg statusMsg) {
    createStatusMsg(statusMsg, StatusMsgType::INFO);
}

void Server::createStatusMsg(StatusMsg statusMsg, StatusMsgType::Type type) {
    statusMsg.addMsg("Server");
    statusMsg.setType(type);
    emit signCreatedStatusMsg(statusMsg);
}

void Server::createStatusMsg(std::string data, StatusMsgType::Type type) {
    createStatusMsg(StatusMsg(data), type);
}

void Server::slotEraseClient(int descriptor) {
    clients.erase(descriptor);
    emit signClientDeleted(descriptor);
}

void Server::clear() {
    for (auto &client: clients)
        client.second->clear();
}

void Server::write(std::string data) {
    for (auto &client: clients)
        client.second->write(data);
}

void Server::write(QByteArray data) {
    for (auto &client: clients)
        client.second->write(data);
}

void Server::write(std::string data, qintptr descriptor) {
    clients.at(descriptor)->write(data);
}

void Server::slotReaded(QString data) {
    emit signReaded(data);
}
