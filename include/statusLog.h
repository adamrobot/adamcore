#pragma once
#include <QThread>
#include <QObject>

#include "server.h"
#include "statusMsg.h"

class StatusLog: public QObject {
    Q_OBJECT
public:
    StatusLog(int port);
private:
    Server *server;
    int port;
signals:
public slots:
    void slotPrintMsg(StatusMsg);
};
