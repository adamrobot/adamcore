

#define SLAVE_ADDRESS 0xBE //real address will be 5F
#define RED 2
#define BLUE 4
#define GREEN 3
#define RB_COLOR 0x21
#define BG_COLOR 0x23
#define RG_COLOR 0x31
#define RGB_COLOR 0x33

#define CLR_SCR_COMMAND 0xFF
#define OFF_LED_CLOCKWISE 0xFE
#define OFF_LED_ANTICLOCKWISE 0xFD
#define ON_LED_CLOCKWISE 0xFC
#define ON_LED_ANTICLOCKWISE 0xFB
#define PLAY_ANIMATION 0xFA
#define SET_ADDRESS 0x0A
#define ON_LED_NOW 0xF9

#define COMMAND_BYTE 0
#define COLOR_BYTE 1
#define START_LED_BYTE 2
#define FINISH_LED_BYTE 3
#define STEP_LED_BYTE 4
#define DELAY_LED_BYTE 5
#define DELAY_FRAME_BYTE 6

#define SPIRAL_ANIM 0x01
#define FAN_ANIM 0x02

extern "C" {
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <inttypes.h>
	#include <errno.h>
}

uint8_t *clrScreen();
uint8_t *onLedClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
uint8_t *onLedAntiClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
uint8_t *offLedClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
uint8_t *offLedAntiClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
uint8_t *playAnim(uint8_t color, uint8_t anim, uint8_t repCount, uint8_t delayLED, uint8_t delayFrame);
uint8_t *setAddress(uint8_t address);
uint8_t get_color(uint8_t color);


uint8_t *setAddress(uint8_t address){
	uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = SET_ADDRESS;
  mymessage[2] = address;
  for (int i = 3; i < 8; ++i)
  {
  	mymessage[i]=0;
  }
  return mymessage;
}


uint8_t get_color(uint8_t color){
switch(color){
    case RED:{
      return RED;
      break;
    }
    case BLUE:{
      return BLUE;
      break;
    }
    case GREEN:{
      return GREEN;
      break;
    }
    case RB_COLOR:{
      return RB_COLOR;
      break;
    }
    case RG_COLOR:{
      return RG_COLOR;
      break;
    }
    case BG_COLOR:{
      return BG_COLOR;
      break;
    }
    case RGB_COLOR:{
      return RGB_COLOR;
      break;
    }
    default:
    return 0x00;
    break;
  }

}

uint8_t *onLedNow(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = ON_LED_NOW;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED)
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t *onLedClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = ON_LED_CLOCKWISE;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED)
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t  *clrScreen(){
	uint8_t *mymessage = (uint8_t *) malloc(8);
	mymessage[0] = 0xAA;
  	mymessage[1] = CLR_SCR_COMMAND;
  	for (int i = 2; i <= 8; ++i)
  	{
  		mymessage[i]=0;
  	}
  	return mymessage;
}

uint8_t  *onLedAntiClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = ON_LED_ANTICLOCKWISE;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED)
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t  *offLedClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = OFF_LED_CLOCKWISE;
  mymessage[2] = 0x00; //color doesnt matter
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED && (finishLED-startLED>=0))
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t  *offLedAntiClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = OFF_LED_ANTICLOCKWISE;
  mymessage[3] = 0x00;
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED && (startLED-finishLED>=0))
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

//uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame
uint8_t  *playAnim(uint8_t color, uint8_t anim, uint8_t repCount, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = PLAY_ANIMATION;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (anim)
  {
    mymessage[3] = anim;
  }
  else
  {
    return 0;
  }
  mymessage[4] = 0x00;
  mymessage[5] = repCount;
  mymessage[6] = delayLED; //0-255 - correct value, so no need to check
  mymessage[7] = delayFrame; //same
  return mymessage;
}
