import asyncore
import socket
import sys
import pyaudio
import wave
import time


CHUNK =  49152#131072 #15036
BUFFER = 49152#131072
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
WIDTH = 2
#data = 0

# output = open('default.audio', 'w')

p = pyaudio.PyAudio()

#wf = wave.open(BUFFER, 'rb')

stream = p.open(format=p.get_format_from_width(WIDTH),
                channels=CHANNELS,
                rate=RATE,
                output=True,
                frames_per_buffer=CHUNK)
#                stream_callback=callback)

stream.start_stream()

class Echoer(asyncore.dispatcher_with_send):
    def handle_read(self):
        data = self.recv(BUFFER)
        stream.write(data)
        # print(data.decode(), file=output)

class EchoServer(asyncore.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(('0.0.0.0', 7000))
        self.listen(1)
        print('waiting for a connection')
        print ("STOP")

    def handle_accept(self):
        sock, addr = self.accept()
        print('connection from', addr)
        print ("PlAYING...")
        handler = Echoer(sock)


server = EchoServer()
asyncore.loop()
