import socket
import sys

host = '127.0.0.1'
servo_port = 6000
led_port   = 6100

servo_command = "{ \"id\": 0, \"command\": 6, \"fields\": {}}"
led_command = "{ \"id\": 0, \"command\": 10, \"fields\": {}}"

count = 1


if len(sys.argv) > 1:
    count = int(sys.argv[1])

def make_data(command):
    data = bytearray()
    data.append(0)
    data.append(0)
    data.append(0)
    data.append(len(command))
    b = bytearray()
    b.extend(map(ord, command))
    data += b
    return data

def make_socket(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    return s

servo_socket = make_socket(host, servo_port)
led_socket   = make_socket(host, led_port)

for i in range(count):
    servo_socket.sendall(make_data(servo_command))
    led_socket.sendall(make_data(led_command))

print("sended")
