#include <QCoreApplication>
#include <iostream>
#include <thread>

#include "core.h"

using namespace std;

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    Core core;

    return app.exec();
}
