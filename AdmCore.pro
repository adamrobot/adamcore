QT += core network serialport multimedia
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#QMAKE_CC = /usr/local/bin/gcc-9
#QMAKE_CXX = /usr/local/bin/g++-9
#QMAKE_CXXFLAGS -= -stdlib=libc++

TARGET = AdmCore
QMAKE_CXXFLAGS += -g -std=c++11

CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

#DEFINES += "WINVER=0x0500"
#DEFINES += "PWD=$$PWD"

INCLUDEPATH += $$PWD/include $$PWD/dynamixel/include/dynamixel_sdk

OTHER_FILES += config/* scripts/*

SOURCES += main.cpp $$PWD/src/*.cpp
HEADERS += $$PWD/include/*.h

macx {
    DEFINES += PORT1=\\\"/dev/ttyUSB0\\\" PORT2=\\\"/dev/ttyUSB1\\\"
    INCLUDEPATH += /usr/local/include/ /usr/local/include/opencv4
    DEFINES += CONFIG=\\\"$$PWD/config/config_mac\\\"
    LIBS += -L/usr/local/lib
    LIBS += -ldxl_mac_cpp
    #LIBS += -lopencv_highgui \
    #        -lopencv_videoio \
    #        -lopencv_core    \
    #        -lopencv_imgproc \
    #        -lopencv_imgcodecs
}

linux {
    QMAKE_CXX = /usr/bin/g++
    DEFINES += PORT1=\\\"/dev/ttyUSB1\\\" PORT2=\\\"/dev/ttyUSB0\\\"
    DEFINES += CAM1=\\\"/dev/video0\\\" CAM2=\\\"/dev/video1\\\"
    INCLUDEPATH += /usr/local/include/ /usr/local/include/opencv4
    DEFINES += CONFIG=\\\"$$PWD/config/config_linux\\\"
    LIBS += -L/usr/local/lib
    LIBS += -ldxl_x86_cpp
    #LIBS += -lopencv_highgui \
    #        -lopencv_videoio \
    #        -lopencv_core    \
    #        -lopencv_imgproc \
    #        -lopencv_imgcodecs

    CONFIG += link_pkgconfig
    PKGCONFIG += gstreamer-1.0
    LIBS += -lgstbase-1.0 -lgstreamer-1.0
}

windows {
    DEFINES += PORT1=\\\"COM1\\\" PORT2=\\\"COM2\\\"
    INCLUDEPATH += D:/IlyaZuev/downloads/rapidjson-1.1.0/rapidjson-1.1.0/include
    #INCLUDEPATH += D:/IlyaZuev/downloads/opencv/build/include
    LIBS += -LD:/IlyaZuev/downloads/DynamixelSDK-3.6.2/DynamixelSDK-3.6.2/c++/build/win64/output
    LIBS += -ldxl_x64_cpp
    #LIBS += -LD:/IlyaZuev/programms/opencv-build/install/x86/mingw/bin
    #LIBS += -lopencv_highgui400 \
    #        -lopencv_videoio400 \
    #        -lopencv_core400    \
    #        -lopencv_imgproc400 \
    #        -lopencv_imgcodecs400
}

#QT_CONFIG -= no-pkg-config
#CONFIG += link_pkgconfige
#PKGCONFIG += gstreamer-1.0
#LIBS += `pkg-config --cflags gstreamer-1.0` `pkg-config --libs gstreamer-1.0`

#QT_CONFIG -= no-pkg-config

#CONFIG += link_pkgconfig
#PKGCONFIG += gstreamer-1.0
#LIBS += -lgstbase-1.0 -lgstreamer-1.0
#INCLUDEPATH += /usr/local/include/gstreamer-1.0 /usr/local/include/glib-2.0


#INCLUDEPATH += /usr/local/include/gstreamer-1.0 /usr/local/Cellar/glib/2.58.1/include /usr/local/Cellar/glib/2.58.1/include/glib-2.0

#LIBS += -lavdevice -lavformat -lavcodec -lswresample -lswscale -lavutil

#LIBS += -L/usr/local/lib
#LIBS += -lportaudio

#DEFINES += QT_DEPRECATED_WARNINGS
