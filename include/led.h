#pragma once
#include <QThread>
#include <QMutex>
#include <memory>
#include <thread>
#include <tuple>
#include <vector>
#include <map>
#include <fstream>

#include "hub.h"
#include "statusMsg.h"
#include "odinEye.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

namespace DiodeColor {
enum Type {
    NONE  = -1,
    RED   =  0,
    GREEN =  1,
    BLUE  =  2
};
}

namespace LedAddress {
enum Type {
    LEFT  = 0,
    RIGHT = 1
};
}

static std::map<LedAddress::Type, int> ledAddress = {
        { LedAddress::LEFT,  0x5f },
        { LedAddress::RIGHT, 0x5c },
};

class Led : public QObject { //make friend with hubcontroller and make public functions private
    Q_OBJECT
public:
    QMutex mutex;
    Led();
    void init(LedAddress::Type address);
    void parseMsg(QString data);
    void setDiodes(int startDiode, int endDiode, DiodeColor::Type);
    void writeToLed(uint8_t* msg);
private:
    QString currentData;
    int i2c;
    LedAddress::Type address;
//    void ledLoop();
//    void didUploadScriptMsg(QString data);
signals:
//    void signReaded(QByteArray);
    void signCreatedStatusMsg(StatusMsg);
public slots:
//    void slotClearRepeatMsgs(int);
    void ledLoop();
//    void slotParseMsg(QString);
//    void slotRepeatMsgs();
};

class LedController : public QObject {
    Q_OBJECT
public:
    std::map<int, std::set<std::pair<int, std::vector<std::tuple<int, int, DiodeColor::Type>>>>> currentScript;
    std::map<int, std::vector<std::tuple<int, int, DiodeColor::Type>>> defaultState;
    int scriptMaxTime;
//    int scriptAddr;
//    int scriptSize;
    Led *leftLed;
    Led *rightLed;
//    void run();
    void init();
    LedController();
    std::fstream file;
private:
    void didUploadScriptMsg(QString data);
    void didRunScriptMsg(QString data);
    void didDefaultLedsMsg(QString data);
//    std::vector<std::string> deviceNames;
//    float protocolVersion;
//    int baudRate;
signals:
//    void signReaded(QByteArray);
//    void signCreatedStatusMsg(StatusMsg);
public slots:
    void slotParseMsg(QString);
};
