import sys

import board
import busio
import time
import threading
import adafruit_motor.servo
import adafruit_pca9685

i2c = busio.I2C(board.SCL, board.SDA)

#cpddvfu305 - CPD2.4 wifi

import adafruit_pca9685
pca = adafruit_pca9685.PCA9685(i2c, address=0x40)
pca.frequency = 50

servo_channel = pca.channels[8]
servo = adafruit_motor.servo.Servo(servo_channel)
servo.angle = 0

servo_channel = pca.channels[10]
servo = adafruit_motor.servo.Servo(servo_channel)
servo.angle = 180

# 125 - 180 // 10 - right
# 0 - 65    // 8  - left
