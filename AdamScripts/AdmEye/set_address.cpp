#include <iostream>
#include <map>

#include "odin_eye.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>

using namespace std;

int i2c;

int main(int argc, char **argv) {
    if ((i2c = open("/dev/i2c-1", O_RDWR)) < 0)
       cout << "Can not open i2c " << i2c << endl;
    else
       cout << "Open i2c " << i2c << endl;

    int ioc;
    if ((ioc = ioctl(i2c, I2C_SLAVE, 0x5f)) < 0)
        cout << "Can not set i2c device address " << address << endl;
    else
        cout << "Open i2c device address " << address << endl;

    uint8_t* msg = setAddress(0xb9);
    write(i2c, msg, 8);

    return 0;
}
