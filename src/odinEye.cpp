#include "odinEye.h"

using namespace RGBLed;

uint8_t* RGBLed::setAddress(uint8_t address){
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = SET_ADDRESS;
  mymessage[2] = address;
  for (int i = 3; i < 8; ++i)
  {
    mymessage[i]=0;
  }
  return mymessage;
}


uint8_t RGBLed::get_color(uint8_t color){
switch(color){
    case RGBLED_RED:{
      return RGBLED_RED;
      break;
    }
    case RGBLED_BLUE:{
      return RGBLED_BLUE;
      break;
    }
    case RGBLED_GREEN:{
      return RGBLED_GREEN;
      break;
    }
    case RB_COLOR:{
      return RB_COLOR;
      break;
    }
    case RG_COLOR:{
      return RG_COLOR;
      break;
    }
    case BG_COLOR:{
      return BG_COLOR;
      break;
    }
    case RGB_COLOR:{
      return RGB_COLOR;
      break;
    }
    default:
    return 0x00;
    break;
  }

}



uint8_t* RGBLed::onLedClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = ON_LED_CLOCKWISE;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED)
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t* RGBLed::clrScreen(){
    uint8_t *mymessage = (uint8_t *) malloc(8);
    mymessage[0] = 0xAA;
    mymessage[1] = CLR_SCR_COMMAND;
    for (int i = 2; i <= 8; ++i)
    {
        mymessage[i]=0;
    }
    return mymessage;
}

uint8_t* RGBLed::onLedAntiClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = ON_LED_ANTICLOCKWISE;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED)
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t* RGBLed::offLedClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = OFF_LED_CLOCKWISE;
  mymessage[2] = 0x00; //color doesnt matter
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED && (finishLED-startLED>=0))
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

uint8_t* RGBLed::offLedAntiClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = OFF_LED_ANTICLOCKWISE;
  mymessage[3] = 0x00;
  if (startLED)
  {
    mymessage[3] = startLED;
  }
  else
  {
    return 0;
  }
  if (finishLED && (startLED-finishLED>=0))
  {
    mymessage[4] = finishLED;
  }
  else
  {
    return 0;
  }
  if (stepLED&&stepLED<=80)
  {
    mymessage[5]=stepLED;
  }
  else
  {
    return 0;
  }
  mymessage[6]=delayLED; //0-255 - correct value, so no need to check
  mymessage[7]=delayFrame; //same
  return mymessage;
}

//uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame
uint8_t* RGBLed::playAnim(uint8_t color, uint8_t anim, uint8_t repCount, uint8_t delayLED, uint8_t delayFrame)
{
  uint8_t *mymessage = (uint8_t *) malloc(8);
  mymessage[0] = 0xAA;
  mymessage[1] = PLAY_ANIMATION;
  uint8_t sColor = get_color(color);
  if (sColor)
  {
  mymessage[2] = sColor;
  }
  else
  {
    return 0;
  }
  if (anim)
  {
    mymessage[3] = anim;
  }
  else
  {
    return 0;
  }
  mymessage[4] = 0x00;
  mymessage[5] = repCount;
  mymessage[6] = delayLED; //0-255 - correct value, so no need to check
  mymessage[7] = delayFrame; //same
  return mymessage;
}
