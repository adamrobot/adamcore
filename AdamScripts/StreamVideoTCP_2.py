import cv2
import numpy as np
import socket
import sys
import pickle
import struct

capL = cv2.VideoCapture(0)
#capR = cv2.VideoCapture(1)

clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket.connect(('192.168.9.68', 8083))

while True:
    ret,frameL = capL.read()
    #ret,frameR = capR.read()
    
    dataL = pickle.dumps(frameL)
    clientsocket.sendall(struct.pack("L", len(dataL)) + dataL)
    
    #dataR = pickle.dumps(frameR)
    #clientsocket.sendall(struct.pack("R", len(dataR)) + dataR)