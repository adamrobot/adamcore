import asyncore
import socket
import sys
import wave
import time
import json

import board
import busio
import time
import threading
from adafruit_motor import motor
import adafruit_pca9685

i2c = busio.I2C(board.SCL, board.SDA)

pca = adafruit_pca9685.PCA9685(i2c, address=0x40)
pca.frequency = 1000

left_forward, right_forward, left_back, right_back = 1, 2, 3, 4

wheels = {
left_forward: [5, 4],
right_forward: [3, 2],
left_back: [6, 7],
right_back: [0, 1]
}
# wheels = [[2, 3], [1, 0], [4, 5], [7, 6]]

def set_velocity(ch1, ch2, value):
    channel1 = pca.channels[ch1]
    channel2 = pca.channels[ch2]
    motor1 = motor.DCMotor(channel1, channel2)
    motor1.throttle = value

def set_velocity_by_wheel(wheel, value):
    global wheels
    set_velocity(wheels[wheel][0], wheels[wheel][1], value)

WRITE = 0
UPLOAD_SCRIPT = 16
RUN_SCRIPT = 14
BUFFER = 4915200
max_time = 0
script = None
current_data = b''
current_size = -1

class Echoer(asyncore.dispatcher_with_send):
    def handle_read(self):
        global script, max_time, current_data, current_size

        data = self.recv(BUFFER)
        if (len(data) == 0):
            return
        print(len(data))
        if current_size == -1:
            current_size = int.from_bytes(data[0:4], byteorder='big')
            data = data[4:]
        current_data += data

        if len(current_data) != current_size:
            return

        data = json.loads(current_data)
        current_data = b''
        current_size = -1

        if data["command"] == WRITE:
            print(data)
            fields = data["fields"]
            for key, value in fields.items():
                set_velocity_by_wheel(int(value["id"] + 1), value["speed"])
            time.sleep(0.5)
            for key, value in fields.items():
                set_velocity_by_wheel(int(value["id"] + 1), 0)

        if data["command"] == UPLOAD_SCRIPT:
            max_time = 0
            script = {"1": [], "2": [], "3": [], "4": []}
            fields = data["fields"]
            print(fields)
            for key, value in fields.items():
                script[key] = value["units"]
                for v in value["units"]:
                    max_time = max(max_time, v["Item1"])

            for key, value in script.items():
                if (len(value) > 0):
                    value.append(value[-1]);
            print(script)
            print(max_time)

        if data["command"] == RUN_SCRIPT and script != None:
            for t in range(max_time + 1):
                for key, value in script.items():
                    if len(value) == 0:
                        continue
                    for j in range(len(value) - 1):
                        if t >= value[j]["Item1"] and t <= value[j + 1]["Item1"]:
                            if value[j + 1]["Item1"] != value[j]["Item1"]:
                                speed = value[j]["Item3"][0]["speed"] + (t - value[j]["Item1"]) * (value[j + 1]["Item3"][0]["speed"] - value[j]["Item3"][0]["speed"]) / (value[j + 1]["Item1"] - value[j]["Item1"])
                                # print(int(key) - 1, " ", speed)
                                set_velocity_by_wheel(int(key), speed)
                                break
                time.sleep(0.01)
            print("Script end")


class EchoServer(asyncore.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(('0.0.0.0', 6200))
        self.listen(1)
        print('Wheels server: waiting for a connection')

    def handle_accept(self):
        sock, addr = self.accept()
        print('connection from', addr)
        handler = Echoer(sock)


server = EchoServer()
asyncore.loop()
