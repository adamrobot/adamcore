#pragma once
#include <string>
#include <vector>
#include <map>
#include <set>
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QString>
#include <stdint.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <sstream>

#include "timer.h"
#include "statusMsg.h"

#include "dynamixel_sdk.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

namespace FieldType {
enum Type {
    NUMBER     = 0,
    ENUM       = 1,
    SERVOFIELD = 2
};
}

namespace Command {
enum Type {
    WRITE              = 0,
    REPEATREAD         = 1,
    READ               = 3,
    STOPREAD           = 4,
    UPLOADSERVOSCRIPT  = 5,
    RUNSERVOSCRIPT     = 6,
    STOPSERVOSCRIPT    = 7,
    SETLED             = 8,
    UPLOADLEDSCRIPT    = 9,
    RUNLEDSCRIPT       = 10,
    STOPLEDSCRIPT      = 11,
    DEFAULTSERVO       = 12,
    DEFAULTLEDS        = 13,
    RUNWHEELSSCRIPT    = 14,
    STOPWHEELSSCRIPT   = 15,
    UPLOADWHEELSSCRIPT = 16,
};
}

//namespace ReadMode {
//enum Type {
//  RECEIVE,
//  TRANSMIT
//};
//}

class Hub : public QObject { //make friend with hubcontroller and make public functions private
    Q_OBJECT
public:
    Hub();
    Hub(std::vector<std::string> deviceNames, std::vector<float> protocolVersions, std::vector<int> baudRates);
    void initGSW(uint16_t startAddress, uint16_t dataLength);
    void addParamGSW(uint8_t id, uint8_t *data);
    void writeGSW();
    void clearGSW();
    int read(uint8_t id, uint16_t address, uint16_t length, uint8_t *data);
    void init(std::vector<std::string> deviceNames, std::vector<float> protocolVersions, std::vector<int> baudRates);
    int getHomePosition(int id);
    std::map<std::string, std::map<int, int>> homePositions;
private:
    std::map<int, std::set<std::pair<int, std::pair<int16_t, int16_t>>>> currentScript;
    std::map<int, std::pair<int16_t, int16_t>> defaultState;
    std::vector<dynamixel::PortHandler*> portHandlers;
    std::vector<dynamixel::PacketHandler*> packetHandlers;
    std::vector<dynamixel::GroupSyncWrite*> groupSyncWrites;
    std::map<int, QString> repeatMsgs;
    Timer *timer;
    int scriptMaxTime;
    int scriptAddr;
    int scriptSize;
    std::vector<int> handsIds = { 245, 246, 244, 243, 249, 250, 248, 247 };
    std::vector<int> allIds = { 242, 241, 240, 236, 235, 234, 239, 238, 237, 2, 1, 245, 246, 244, 243, 249, 250, 248, 247 };
    std::vector<int> anotherIds = { 242, 241, 240, 236, 235, 234, 239, 238, 237, 2, 1 };

    QByteArray readServoData(QString data);
    void didWriteMsg(QString data);
    void didUploadScriptMsg(QString data);
    void didRunScriptMsg(QString data);
    void didDefaultServoMsg(QString data);
    void didReadMsg(QString data);
    void didStopRepeatReadMsg(QString data);
    void createStatusMsg(std::string, StatusMsgType::Type type = StatusMsgType::INFO);
    uint8_t* createAngleAndSpeedParams(int16_t angle, int16_t speed);
    void didScriptIterate(int time, std::vector<int> ids);
    void didDefaultIterate(int time, std::vector<int> ids);
signals:
    void signReaded(QByteArray);
    void signCreatedStatusMsg(StatusMsg);
public slots:
    void slotClearRepeatMsgs(int);
    void slotParseMsg(QString);
    void slotRepeatMsgs();
};

class HubController : public QThread {
    Q_OBJECT
public:
    Hub *hub;
    void run();
    void init();
    HubController(std::vector<std::string> deviceNames, std::vector<float> protocolVersions, std::vector<int> baudRates);
private:
    std::vector<std::string> deviceNames;
    std::vector<float> protocolVersions;
    std::vector<int> baudRates;
};
