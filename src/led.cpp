#include "led.h"

using namespace std;
using namespace rapidjson;

Led::Led() {
//    timer = new Timer();

//    connect(timer, SIGNAL(finished()), timer, SLOT(deleteLater()));
//    connect(timer, SIGNAL(signTimed()), this, SLOT(slotRepeatMsgs()));

//    timer->start();;
//    QThread thread = new QThread(ledLoop);
//    thread.start();
//    QThread thread;
//    moveToThread(&thread);
//    connect(&thread, SIGNAL(started()), this, SLOT(slotLedLoop()));
//    thread.start();
}

void Led::init(LedAddress::Type address) {
////    QThread thread;
////    moveToThread(&thread);
////    connect(&thread, SIGNAL(started()), this, SLOT(slotLedLoop()));
////    thread.start();

//    QThread thread;
//    QObject::connect(&thread, SIGNAL(started()), this, SLOT(ledLoop()));
////    QObject::connect(&thread, SIGNAL(finished()), &a, SLOT(quit()));

//    this->moveToThread(&thread);
//    thread.start();

////    std::thread ledLoopThread(ledLoop);
    this->address = address;
    if ((i2c = open("/dev/i2c-1", O_RDWR)) < 0)
      qDebug() << "Can not open i2c " << i2c;
    else
      qDebug() << "Open i2c " << i2c;

    int ioc;
    if ((ioc = ioctl(i2c, I2C_SLAVE, ledAddress.at(address))) < 0)
       qDebug() << "Can not set i2c device address" << address;
    else
       qDebug() << "Open i2c device address" << address;

    // writeToLed(RGBLed::onLedClockwise(RGBLED_GREEN, 1, 20, 1, 0, 0));
    // std::thread ledLoopThread(&Led::ledLoop, this);
    // ledLoopThread.detach();
}

void Led::writeToLed(uint8_t* msg) {
    write(i2c, msg, 8);
    uint8_t answer = -1;
    while (answer != 0) read(i2c, &answer, 1);
}

void Led::ledLoop() {
//    while (true) {
//        rapidjson::Document document;

//        mutex.lock();
//        if (currentData == "") {
//            mutex.unlock();
//            continue;
//        }
//        // qDebug() << "Ok";
//        document.Parse(currentData.toStdString().data());
//        mutex.unlock();

//        document.GetObject();

//        switch (document["command"].GetInt()) {
//        case Command::SETLED: {
//           // writeToLed(RGBLed::clrScreen());
//           for (auto &field: document["fields"].GetObject()) {
//               Value &value = field.value;

//               int startDiode = value["startDiode"].GetInt() + 1;
//               int endDiode   = value["endDiode"].GetInt() + 1;
//               int color      = value["color"].GetInt();

//               // qDebug() << startDiode << " " << endDiode << " " << color;
//               writeToLed(RGBLed::clrScreen());
//               switch (color) {
//               case DiodeColor::RED: {
//                   writeToLed(RGBLed::onLedClockwise(RGBLED_RED, startDiode, endDiode, 1, 0, 0));
//                   break;
//               }
//               case DiodeColor::GREEN: {
//                   writeToLed(RGBLed::onLedClockwise(RGBLED_GREEN, startDiode, endDiode, 1, 0, 0));;
//                   break;
//               }
//               case DiodeColor::BLUE: {
//                   writeToLed(RGBLed::onLedClockwise(RGBLED_BLUE, startDiode, endDiode, 1, 0, 0));
//                   break;
//               }
//               }
//           }
//           break;
//        }
//        }
//    }
}

void Led::setDiodes(int startDiode, int endDiode, DiodeColor::Type color) {
//    qDebug() << startDiode << " " << endDiode << " " << color;
    switch (color) {
    case DiodeColor::RED: {
        writeToLed(RGBLed::onLedClockwise(RGBLED_RED, startDiode, endDiode, 1, 0, 0));
        break;
    }
    case DiodeColor::GREEN: {
        writeToLed(RGBLed::onLedClockwise(RGBLED_GREEN, startDiode, endDiode, 1, 0, 0));
        break;
    }
    case DiodeColor::BLUE: {
        writeToLed(RGBLed::onLedClockwise(RGBLED_BLUE, startDiode, endDiode, 1, 0, 0));
        break;
    }
    }
}

void Led::parseMsg(QString data) {
    currentData = data;

    rapidjson::Document document;
    try {
        document.Parse(currentData.toStdString().data());
        document.GetObject();
    }
    catch(...) {
        return;
    }

    writeToLed(RGBLed::clrScreen());
    for (auto &field: document["fields"].GetObject()) {
        Value &value = field.value;

        int startDiode = value["startDiode"].GetInt() + 1;
        int endDiode   = value["endDiode"].GetInt() + 1;
        int color      = value["color"].GetInt();

        setDiodes(startDiode, endDiode, (DiodeColor::Type)color);
    }
}

LedController::LedController() {
    leftLed = new Led();
    rightLed = new Led();

    init();

    // {
    //     fstream file("default.led");
    //     std::stringstream buffer;
    //     buffer << file.rdbuf();
    //     file.close();
    //     if (buffer.str().size() == 0) return;
    //     QString data = QString::fromStdString(buffer.str());
    //     didDefaultLedsMsg(data);
    // }

    {
        fstream file("script.led");
        std::stringstream buffer;
        buffer << file.rdbuf();
        file.close();
        if (buffer.str().size() == 0) return;
        QString data = QString::fromStdString(buffer.str());
        didUploadScriptMsg(data);
    }
}

//void LedController::run() {
//    leftLed = new Led();
//    rightLed = new Led();
////    exec();
//}

void LedController::init() {
    leftLed->init(LedAddress::LEFT);
    rightLed->init(LedAddress::RIGHT);
}

void LedController::slotParseMsg(QString data) {
    rapidjson::Document document;
//    qDebug() << "true";
//    return;
    try {
        document.Parse(data.toStdString().data());
        document.GetObject();
    }
    catch(...) {
        return;
    }

    switch (document["command"].GetInt()) {
    case Command::SETLED: {
        if (document["type"].GetInt() == LedAddress::LEFT)
            leftLed->parseMsg(data);
        else if (document["type"].GetInt() == LedAddress::RIGHT)
            rightLed->parseMsg(data);
        break;
    }
    case Command::UPLOADLEDSCRIPT: {
        didUploadScriptMsg(data);
        break;
    }
    case Command::RUNLEDSCRIPT: {
        didRunScriptMsg(data);
        break;
    }
    case Command::DEFAULTLEDS: {
        didDefaultLedsMsg(data);
        break;
    }
    }
}

void LedController::didDefaultLedsMsg(QString data) {
//    didDefaultLedsMsg
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    defaultState.clear();

    for (auto &field: document["fields"].GetObject()) {
        Value &fieldValue = field.value;
        for (auto &unit: fieldValue["units"].GetArray()) {
            int type = unit["Item1"].GetInt();
            std::vector<std::tuple<int, int, DiodeColor::Type>> diodes;
            for (auto &servo: unit["Item2"].GetArray()) {
                int startDiode = servo["startDiode"].GetInt() + 1;
                int endDiode   = servo["endDiode"].GetInt() + 1;
                int color      = servo["color"].GetInt();
                diodes.push_back(std::tuple<int, int, DiodeColor::Type>(startDiode, endDiode, (DiodeColor::Type)color));
            }
            defaultState[type] = diodes;
        }
    }

    fstream file("default.led", std::ofstream::out | std::ofstream::trunc);
    file << data.toStdString();
    file.close();
    qDebug() << "upload default leds " << defaultState.size();
}

void LedController::didRunScriptMsg(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    qDebug() << "Run led script " << currentScript.size();
    if (currentScript.size() == 0) return;
    //(time, (angle, speed)))
//std::map<int, std::set<std::pair<int, std::vector<std::tuple<int, int, DiodeColor::Type>>>>> currentScript;
    int currentTime = 0;
    Led* currentLed;
    while (currentTime != scriptMaxTime) {
        currentTime++;
        for (auto &entry: currentScript) {
            if (LedAddress::LEFT == entry.first)
                currentLed = leftLed;
            else if (LedAddress::RIGHT == entry.first)
                currentLed = rightLed;
            else
                continue;

            auto right = entry.second.begin();
            auto left = right;
            while (true) {
                if (currentTime == (*left).first) {
                    // currentTime = (*right).first;
                    currentLed->writeToLed(RGBLed::clrScreen());
                    for (int i = 0; i < ((*left).second).size(); i++)
                        currentLed->setDiodes(std::get<0>((*left).second[i]), std::get<1>((*left).second[i]), std::get<2>((*left).second[i]));

                    break;
                }
                if (right == entry.second.end())
                    break;
                right++;
                left = prev(right, 1);
            }
        }
        usleep(10000);
    }
    qDebug() << "end led script";

    if (defaultState.size() == 0) return;

    qDebug() << "start return to default led";

    int endTime = 200;
    for (int time = 0; time <= endTime; time++) {
        for (auto &entry: defaultState) {
            if (LedAddress::LEFT == entry.first)
                currentLed = leftLed;
            else if (LedAddress::RIGHT == entry.first)
                currentLed = rightLed;
            else
                continue;
//std::map<int, std::vector<std::tuple<int, int, DiodeColor::Type>>> defaultState;
            // if (currentScript.find(entry.first) == currentScript.end()) continue;
            // auto lastValue = (currentScript.at(entry.first)).end();
            // lastValue--;
            if (time == endTime) {
                currentLed->writeToLed(RGBLed::clrScreen());
                for (int i = 0; i < entry.second.size(); i++) {
                    currentLed->setDiodes(std::get<0>(entry.second[i]), std::get<1>(entry.second[i]), std::get<2>(entry.second[i]));
                }
            }
        }
        usleep(10000);
    }

    qDebug() << "end return to default led";
}

void LedController::didUploadScriptMsg(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    currentScript.clear();
    scriptMaxTime = 0;
//std::map<int, std::set<std::pair<int, std::vector<std::tuple<int, int, DiodeColor::Type>>>>> currentScript;
    for (auto &field: document["fields"].GetObject()) {
        Value &fieldValue = field.value;
        for (auto &unit: fieldValue["units"].GetArray()) {
            int time = unit["Item1"].GetInt();
            int type = unit["Item2"].GetInt();
            std::vector<std::tuple<int, int, DiodeColor::Type>> diodes;
            scriptMaxTime = max(scriptMaxTime, time);
            for (auto &servo: unit["Item3"].GetArray()) {
//                scriptAddr = servo["addr"].GetInt();
//                scriptSize = servo["size"].GetInt();

                int startDiode = servo["startDiode"].GetInt() + 1;
                int endDiode   = servo["endDiode"].GetInt() + 1;
                int color      = servo["color"].GetInt();

                diodes.push_back(std::tuple<int, int, DiodeColor::Type>(startDiode, endDiode, (DiodeColor::Type)color));
            }
            currentScript[type].emplace(make_pair(time, diodes));
        }
    }

    fstream file("script.led", std::ofstream::out | std::ofstream::trunc);
    file << data.toStdString();
    file.close();
    qDebug() << "upload led script " << scriptMaxTime;
}
