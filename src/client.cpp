#include "client.h"

using namespace std;

//Client::Client(QObject *parent) :
Client::Client(qintptr descriptor, QObject *parent) :
    QObject(parent) {
    this->descriptor = descriptor;
    blockSize = 0;
    init();
}

QHostAddress Client::peerAddress() {
    return socket->peerAddress();
}

void Client::init() {
    socket = new QTcpSocket();

    createStatusMsg("A new socket created", StatusMsgType::INFO);
//    connect(socket, SIGNAL(connected()), this, SLOT(slotConnected()), Qt::DirectConnection);
//    connect(socket, SIGNAL(disconnected()), this, SLOT(slotDisconnected()), Qt::DirectConnection);
//    connect(socket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()), Qt::DirectConnection);
//    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)), Qt::DirectConnection);

    connect(socket, SIGNAL(connected()), this, SLOT(slotConnected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(slotDisconnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
//    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));

    socket->setSocketDescriptor(descriptor);

//    emit signConnected(this);

    createStatusMsg("Client connected at " + to_string(descriptor), StatusMsgType::SUCCESS);

//    exec();
}

void Client::createStatusMsg(std::string data, StatusMsgType::Type type) {
    StatusMsg statusMsg(data);
    statusMsg.addMsg("Client");
    statusMsg.setType(type);
    emit signCreatedStatusMsg(statusMsg);
}

//void Client::setDescriptor(qintptr descriptor) {
//    this->descriptor = descriptor;
//    socket->setSocketDescriptor(descriptor);
//}

//void Client::connect(const QString &hostName, float port) {
//    socket->connectToHost(hostName, port);
//    descriptor = socket->socketDescriptor();
//}

void Client::slotConnected() {
    createStatusMsg("Client connected event", StatusMsgType::INFO);
}

void Client::slotDisconnected() {
    createStatusMsg("Disconnected", StatusMsgType::INFO);
//    socket->deleteLater();
//    exit(0);
    emit signDeleted(this->descriptor);
}

void Client::clear() {
    socket->readAll();
    blockSize = 0;
}

// void Client::slotReadyRead() {
//     currentData.append(socket->readAll());
//     int index = 0;
//     while (true) {
//         if (blockSize == 0) {
//             if (currentData.size() - index < sizeof(blockSize))
//                 return;
//             blockSize = quint32((unsigned char)(currentData[index])     << 24 |
//                                 (unsigned char)(currentData[index + 1]) << 16 |
//                                 (unsigned char)(currentData[index + 2]) << 8  |
//                                 (unsigned char)(currentData[index + 3]));
//             //qDebug() << blockSize;
//             index += sizeof(blockSize);
//         }
//         if (currentData.size() - index < blockSize) {
//             currentBlock.append(currentData.mid(index, -1));
//             blockSize = blockSize - (currentData.size() - index);
//             currentData.clear();
//             return;
//         }
//         currentBlock.append(currentData.mid(index, blockSize));
//         index += blockSize;
//         blockSize = 0;
//
//         //qDebug() << currentBlock;
//         emit signReaded(currentBlock);
//
//         currentBlock.clear();
//     }
// }

void Client::slotReadyRead() {
    QByteArray data = socket->readAll();

    int index = 0;
    while (true) {
        // if (blockSize > 10000) {
        //     blockSize = 0;
        //     return;
        // }
        if (blockSize == 0) {
            if (data.size() - index < sizeof(blockSize))
                return;
            currentBlock.clear();
            blockSize = quint32((unsigned char)(data[index])     << 24 |
                                (unsigned char)(data[index + 1]) << 16 |
                                (unsigned char)(data[index + 2]) << 8  |
                                (unsigned char)(data[index + 3]) );
            //qDebug() << blockSize;
            index += sizeof(blockSize);
        }
        if (data.size() - index < blockSize) {
            currentBlock.append(data.mid(index, -1));
            blockSize = blockSize - (data.size() - index);
            return;
        }
        currentBlock.append(data.mid(index, blockSize));
        index += blockSize;
        blockSize = 0;

        //qDebug() << currentBlock;
        emit signReaded(currentBlock);
    }
}

void Client::slotError(QAbstractSocket::SocketError socketError) {
    switch (socketError) {
//        case QAbstractSocket::RemoteHostClosedError: {
//            log.append("Client. Error. Some");
//            qDebug() << "Error. Some";
//            break;
//        }
//        case QAbstractSocket::HostNotFoundError: {
//            log.append("Client. Error. The host was not found");
//            qDebug() << "Error. The host was not found";
//            break;
//        }
//        case QAbstractSocket::ConnectionRefusedError: {
//            log.append("Client. Error. The connection was refused by the peer.");
//            qDebug() << "Error. The connection was refused by the peer.";
//            break;
//        }
        default: {
            createStatusMsg("The following error occurred: " + socket->errorString().toStdString(), StatusMsgType::ERROR);
        }
    }
}

qint64 Client::write(std::string data) {
    if (socket->bytesToWrite() > 0) {
        //createStatusMsg("to write " + to_string(socket->bytesToWrite()));
        qDebug() << "to write " << socket->bytesToWrite();
    }
    /*if (socket->bytesToWrite() > 0)
        return -1;*/
    return socket->write(data.data(), data.size());
}

qint64 Client::write(QByteArray data) {
    if (socket->bytesToWrite() > 0) {
        qDebug() << "to write " << socket->bytesToWrite();
        //createStatusMsg("to write " + to_string(socket->bytesToWrite()));
    }
    /*if (socket->bytesToWrite() > 0)
        return -1;*/
    return socket->write(data);
}
