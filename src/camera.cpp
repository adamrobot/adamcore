//#include "camera.h"
//#include <thread>

//using namespace std;

//Camera::Camera(string device, int port, string address, QObject *parent) :
//  device(device), port(port), address(address), QThread(parent) {}

//void Camera::run() {
//  // createPipeline();
//  // std::thread video1(createPipeline1);
//  // video1.detach();
//  exec();
//}

//void Camera::createPipeline() {
//  qDebug() << "create pipeline";
//  GstBus *bus;
//  GstMessage *msg;
//  GMainLoop *loop;
//  GstStateChangeReturn ret;
//  //initialize all elements
//  gst_init(NULL, NULL);
//  GstElement *pipeline = gst_pipeline_new ("pipeline");
//  GstElement *source = gst_element_factory_make ("v4l2src", "source");
//  //    sink = gst_element_factory_make ("autovideosink", "sink");
//  GstElement *filter = gst_element_factory_make("capsfilter", "filter");
//  GstElement *rtppay = gst_element_factory_make("rtpjpegpay", "rtppay");
//  GstElement *sink = gst_element_factory_make ("udpsink", "sink");

//  if (!pipeline || !source || !sink) {
//     cout << "not all elements created: pipeline["<< !pipeline<< "]" << "source["<< !source<< "]" << "sink["<< !sink << "]" << endl;
//     return;
//  }

//  g_object_set(G_OBJECT (source),
//              "device", device.c_str(),
//              NULL);

//  GstCaps *caps = gst_caps_new_simple (
//             "image/jpeg",
//             NULL);
//  g_object_set(G_OBJECT (filter), "caps", caps, NULL);

//  g_object_set(G_OBJECT (sink),
//              "host", address.c_str(),
//              "port", port,
//              NULL);


//  gst_bin_add_many(GST_BIN (pipeline), source, filter, rtppay, sink, NULL);
//  if (gst_element_link_many(source, filter, rtppay, sink, NULL) != TRUE) {
//     gst_object_unref(pipeline);
//     return;
//  }

//  ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
//  if (ret == GST_STATE_CHANGE_FAILURE) {
//     gst_object_unref(pipeline);
//     return;
//  }

//  bus = gst_element_get_bus (pipeline);

//  loop = g_main_loop_new(NULL, FALSE);
//  g_main_loop_run(loop);

//  g_main_loop_unref(loop);
//  gst_object_unref (bus);
//  gst_element_set_state (pipeline, GST_STATE_NULL);
//  gst_object_unref (pipeline);
//}
