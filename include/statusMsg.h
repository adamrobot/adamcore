#pragma once

#include <QMetaType>

#include <string>
#include <vector>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

namespace StatusMsgType {
enum Type {
    INFO    = 0,
    SUCCESS = 1,
    WARNING = 2,
    ERROR   = 3
};
}

class StatusMsg {
public:
    StatusMsg();
    StatusMsg(std::string msg);
    void addMsg(const std::string msg);
    std::string getResultMsg();
    std::string getData();
    void setType(const StatusMsgType::Type type);
private:
    std::vector<std::string> msgs;
    StatusMsgType::Type type;
};

Q_DECLARE_METATYPE(StatusMsg)
