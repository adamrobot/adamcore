import sys

import board
import busio
import time
import threading
i2c = busio.I2C(board.SCL, board.SDA)

#cpddvfu305 - CPD2.4 wifi

import adafruit_pca9685
pca = adafruit_pca9685.PCA9685(i2c, address=0x40)
pca.frequency = 1000

from adafruit_motor import motor

wheels = [[2, 3], [1, 0], [4, 5], [7, 6]]

def set_velocity(ch1, ch2, value):
    channel1 = pca.channels[ch1]
    channel2 = pca.channels[ch2]
    motor1 = motor.DCMotor(channel1, channel2)
    motor1.throttle = value

import curses

forward, off, reverse = 1, 0, -1
states = [forward, forward, forward, forward]
velocity = {forward: 1.0, off: 0.0, reverse: -1.0}
keys = {
curses.KEY_UP:    [forward, forward, forward, forward],
curses.KEY_DOWN:  [reverse, reverse, reverse, reverse],
curses.KEY_LEFT:  [reverse, forward, forward, reverse],
curses.KEY_RIGHT: [forward, reverse, reverse, forward],
ord('w'): [forward, reverse, forward, reverse],
ord('s'): [reverse, forward, reverse, forward],
ord('q'): [off, forward, forward, off],
ord('e'): [forward, off, off, forward],
ord('a'): [reverse, off, off, reverse],
ord('d'): [off, reverse, reverse, off]
}

def set_all_velocity(value):
    for wheel in wheels:
        set_velocity(wheel[0], wheel[1], value)

def set_chosen_velocity():
    for wheel_index in range(len(wheels)):
        # print(wheels[wheel_index][0])
        set_velocity(wheels[wheel_index][0], wheels[wheel_index][1], velocity[states[wheel_index]])

def state_name(state):
    return {
        forward: "Forward",
        off:     "Off",
        reverse: "Reverse",
    }[state]

def wheel_name(wheel):
    return {
        0: "Left front",
        1: "Right front",
        2: "Left back",
        3: "Right back",
    }[wheel]

def next_wheel_state(wheel):
    states[wheel] = (states[wheel] + 1) % 3

def write_wheel_state(wheel, row, column):
    stdscr.addstr(row, column, " " * 50)
    stdscr.addstr(row, column, wheel_name(wheel) + ": " + state_name(states[wheel]))

mutex = threading.Lock()
current_speeds = [0, 0, 0, 0]
isRun = True
isPressed = False
max_speed = 1.0
min_speed = -1.0

def set_states(key):
    global isPressed
    if key not in keys.keys():
        return
    for i in range(len(keys[key])):
        # previus_states[i] = states[i]
        states[i] = keys[key][i]
    isPressed = True
    # set_all_velocity(1.0)
    # time.sleep(0.01)
    # set_all_velocity(None)

def set_velocity_by_current_speed():
    for wheel_index in range(len(wheels)):
        # print(wheels[wheel_index][0])
        set_velocity(wheels[wheel_index][0], wheels[wheel_index][1], current_speeds[wheel_index])

def ride(seconds, key):
    for i in range(int(seconds * 20)):
        time.sleep(0.05)
        mutex.acquire()
        set_states(key)
        mutex.release()

def timer():
    global isPressed
    global current_speeds
    global states
    global previus_states
    threshold = 0.0005
    tick_time = 0.1
    boost_time = 0.5
    value = 1.0 * tick_time / boost_time
    while isRun == True:
        mutex.acquire()
        if isPressed:
            isPressed = False
            for i in range(len(current_speeds)):
                if states[i] == forward:
                    current_speeds[i] = max(min(current_speeds[i] + value, max_speed), min_speed)
                elif states[i] == reverse:
                    current_speeds[i] = max(min(current_speeds[i] - value, max_speed), min_speed)
                elif states[i] == off and current_speeds[i] > 0:
                    current_speeds[i] -= value
                elif states[i] == off and current_speeds[i] < 0:
                    current_speeds[i] += value
                # if states[i] == off and abs(current_speeds[i]) < threshold and abs(current_speeds[i]) > -threshold:
                #     current_speeds[i] = 0.0
        else:
            for i in range(len(current_speeds)):
                if current_speeds[i] > threshold:
                    current_speeds[i] -= value
                elif current_speeds[i] < -threshold:
                    current_speeds[i] += value
                else:
                    current_speeds[i] = 0

        set_velocity_by_current_speed()
        mutex.release()
        time.sleep(tick_time)

thread = threading.Thread(target=timer, args=())
thread.start()
# thread.join()

stdscr = curses.initscr()
curses.cbreak()
curses.noecho()
stdscr.keypad(1)
stdscr.nodelay(1)

key = ''

while key == -1 or key != ord('z'):
    key = stdscr.getch()
    stdscr.refresh()

    [stdscr.addstr(i, 0, " " * 40) for i in range(len(current_speeds))]
    [stdscr.addstr(i, 15, str(current_speeds[i])) for i in range(len(current_speeds))]

    if key == -1:
        continue

    stdscr.addstr(8, 10, chr(key))

    mutex.acquire()
    set_states(key)
    mutex.release()
    if (key == ord('v')):
        ride(0.5, ord('w'))
        ride(0.5, ord('s'))
        ride(0.5, curses.KEY_UP)
        ride(0.5, ord('e'))
        ride(0.5, curses.KEY_RIGHT)
        ride(0.5, ord('d'))
        ride(0.5, curses.KEY_DOWN)
        ride(0.5, ord('a'))
        ride(0.5, curses.KEY_LEFT)
        ride(0.5, ord('q'))

    # if key == ord('r'):
    #     set_chosen_velocity()
    #     time.sleep(1)
    #     set_all_velocity(None)
    #
    # for i in range(1, 5):
    #     if key == ord(str(i)):
    #         next_wheel_state(i - 1)

    [write_wheel_state(i, i, 50) for i in range(len(states))]

curses.endwin()

isRun = False
