import asyncore
import socket
import sys
import serial

ser=serial.Serial('/dev/ttyUSB1', 57600)
ser2=serial.Serial('/dev/ttyUSB0', 57600)

class Echoer(asyncore.dispatcher_with_send):
    def handle_read(self):
        data = self.recv(1024)
        self.send(b'Got data...')
        
        ser.write(data)
        ser2.write(data)
        # print('received {!r}'.format(data))
class EchoServer(asyncore.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.bind(('192.168.1.232', 8080))
        self.bind(('192.168.1.103', 8080))
        self.listen(1)
        print('waiting for a connection')
    def handle_accept(self):
        sock, addr = self.accept()
        print('connection from', addr)
        handler = Echoer(sock)
server = EchoServer()
asyncore.loop()