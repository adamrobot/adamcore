import math

for i in range(8):
    angle = i * 45 - 22.5
    x = 1 + 0.5 * math.cos(angle * math.pi / 180)
    y = 1 + 0.5 * math.sin(angle * math.pi / 180)
    print(angle, x, y)
