
import socket
import wave

HOST = '127.0.0.1'
PORT = 5000

run_robot   = b'{"id": 0,"command": 6,"fields": {}}'
run_leds    = b'{"id": 0,"command": 10,"fields": {}}'
run_wheels  = b'{"id": 0,"command": 14,"fields": {}}'
run_fingers = b'{"id": 0,"command": 17,"fields": {}}'

def send(socket, host, port, command):
    s.connect((host, port))
    s.send(len(command).to_bytes(4, byteorder = 'big') + command)


try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        send(s, '127.0.0.1', 5000, run_robot)
except Exception:
    print('cant send robot')

try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        send(s, '127.0.0.1', 6100, run_leds)
except Exception:
    print('cant send leds')

# try:
#     with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#         s.connect(('127.0.0.1', 7000))
#         with wave.open(r'default.audio', 'rb') as f:
#             data = f.readframes(1024)
#             while data:
#                 s.send(data)
#                 data = f.readframes(1024)
# except Exception:
#     print('cant send audio')

try:
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        with open('Fingers/default.fingers', 'rb') as f:
            read_data = f.read()
            send(s, '127.0.0.1', 6300, read_data)
        send(s, '127.0.0.1', 6300, run_fingers)
except Exception:
    print('cant send fingers')

try:
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        with open('default.wheels', 'rb') as f:
            read_data = f.read()
            send(s, '127.0.0.1', 6200, read_data)
        send(s, '127.0.0.1', 6200, run_wheels)
except Exception:
    print('cant send wheels')
