import asyncore
import socket
import sys
import wave
import time
import json

import board
import busio
import time
import threading
import adafruit_motor.servo
import adafruit_pca9685

i2c = busio.I2C(board.SCL, board.SDA)

pca = adafruit_pca9685.PCA9685(i2c, address=0x40)
pca.frequency = 50

left_hand, right_hand = 1, 2
hands = {
left_hand: { "min": 180, "max": 125, "servo": adafruit_motor.servo.Servo(pca.channels[10]) },
right_hand: { "min": 0, "max": 60, "servo": adafruit_motor.servo.Servo(pca.channels[8]) }
}

def set_angle(hand, value):
    global hands
    value = (hands[hand]["max"] - hands[hand]["min"]) * value + hands[hand]["min"]
    # print("hand: ", hand, " ", value)
    hands[hand]["servo"].angle = value

WRITE = 0
UPLOAD_SCRIPT = 19
RUN_SCRIPT = 17
BUFFER = 4915200
max_time = 0
script = None
current_data = b''
current_size = -1

class ServerUDP(asyncore.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.bind(('0.0.0.0', 6300))
        print("Started")

    def handle_connect(self):
        print("Connected")

    def handle_read(self):
        global script, max_time, current_data, current_size

        data = self.recv(BUFFER)
        if (len(data) == 0):
            return
        print(len(data))
        if current_size == -1:
            current_size = int.from_bytes(data[0:4], byteorder='big')
            data = data[4:]
        current_data += data

        if len(current_data) != current_size:
            return

        data = json.loads(current_data)
        tmp = current_data
        current_data = b''
        current_size = -1

        # if data["command"] == WRITE:
        #     print(data)
        #     fields = data["fields"]
        #     for key, value in fields.items():
        #         set_velocity_by_wheel(int(value["id"] + 1), value["speed"])
        #     time.sleep(0.5)
        #     for key, value in fields.items():
        #         set_velocity_by_wheel(int(value["id"] + 1), 0)

        if data["command"] == UPLOAD_SCRIPT:
            max_time = 0
            script = {"1": [], "2": []}
            fields = data["fields"]
            print(fields)
            for key, value in fields.items():
                script[key] = value["units"]
                for v in value["units"]:
                    max_time = max(max_time, v["Item1"])

            for key, value in script.items():
                if (len(value) > 0):
                    value.append(value[-1]);
            print(script)
            print(max_time)
            with open('default.fingers', 'w') as f:
                print(tmp.decode(), file=f)

        if data["command"] == RUN_SCRIPT and script != None:
            for t in range(max_time + 1):
                for key, value in script.items():
                    if len(value) == 0:
                        continue
                    for j in range(len(value) - 1):
                        print("left: ", value[j]["Item1"])
                        print("right: ", value[j + 1]["Item1"])
                        if t >= value[j]["Item1"] and t <= value[j + 1]["Item1"]:
                            if value[j + 1]["Item1"] != value[j]["Item1"]:
                                angle = value[j]["Item3"][0]["angle"] + (t - value[j]["Item1"]) * (value[j + 1]["Item3"][0]["angle"] - value[j]["Item3"][0]["angle"]) / (value[j + 1]["Item1"] - value[j]["Item1"])
                                if int(key) == 1:
                                    print("angle: ", angle, "time: ", t)
                                set_angle(int(key), angle)
                                break
                time.sleep(0.01)
            print("Script end")

    def handle_write(self):
        pass


server = ServerUDP()
asyncore.loop()
