{
  "id": 0,
  "command": 5,
  "fields": {
    "1": {
      "$type": "AdamController.Message.ServoScriptField, AdamController",
      "units": [
        {
          "Item1": 101,
          "Item2": [
            {
              "id": 2,
              "data": [
                0,
                0,
                200,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 1,
              "data": [
                0,
                0,
                200,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        },
        {
          "Item1": 302,
          "Item2": [
            {
              "id": 2,
              "data": [
                0,
                0,
                200,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 1,
              "data": [
                0,
                0,
                200,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        }
      ],
      "type": 1
    },
    "2": {
      "$type": "AdamController.Message.ServoScriptField, AdamController",
      "units": [
        {
          "Item1": 101,
          "Item2": [
            {
              "id": 240,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 241,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 242,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        },
        {
          "Item1": 302,
          "Item2": [
            {
              "id": 240,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 241,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 242,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        }
      ],
      "type": 1
    },
    "3": {
      "$type": "AdamController.Message.ServoScriptField, AdamController",
      "units": [
        {
          "Item1": 101,
          "Item2": [
            {
              "id": 245,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 246,
              "data": [
                237,
                6,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 244,
              "data": [
                170,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 243,
              "data": [
                170,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        },
        {
          "Item1": 302,
          "Item2": [
            {
              "id": 245,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 246,
              "data": [
                237,
                6,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 244,
              "data": [
                170,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 243,
              "data": [
                170,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        }
      ],
      "type": 1
    },
    "4": {
      "$type": "AdamController.Message.ServoScriptField, AdamController",
      "units": [
        {
          "Item1": 101,
          "Item2": [
            {
              "id": 249,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 250,
              "data": [
                19,
                249,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 248,
              "data": [
                86,
                255,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 247,
              "data": [
                86,
                255,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        },
        {
          "Item1": 302,
          "Item2": [
            {
              "id": 249,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 250,
              "data": [
                19,
                249,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 248,
              "data": [
                86,
                255,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 247,
              "data": [
                86,
                255,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        }
      ],
      "type": 1
    },
    "5": {
      "$type": "AdamController.Message.ServoScriptField, AdamController",
      "units": [
        {
          "Item1": 101,
          "Item2": [
            {
              "id": 236,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 235,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 234,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        },
        {
          "Item1": 302,
          "Item2": [
            {
              "id": 236,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 235,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 234,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        }
      ],
      "type": 1
    },
    "6": {
      "$type": "AdamController.Message.ServoScriptField, AdamController",
      "units": [
        {
          "Item1": 101,
          "Item2": [
            {
              "id": 239,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 238,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 237,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        },
        {
          "Item1": 302,
          "Item2": [
            {
              "id": 239,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 238,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            },
            {
              "id": 237,
              "data": [
                0,
                0,
                0,
                0
              ],
              "size": 4,
              "addr": 30,
              "type": 2
            }
          ]
        }
      ],
      "type": 1
    }
  }
}