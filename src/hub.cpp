#include "hub.h"

using namespace std;
using namespace rapidjson;

Hub::Hub() {
    timer = new Timer();

    connect(timer, SIGNAL(finished()), timer, SLOT(deleteLater()));
    connect(timer, SIGNAL(signTimed()), this, SLOT(slotRepeatMsgs()));

    timer->start();
}

Hub::Hub(vector<string> deviceNames, vector<float> protocolVersions, vector<int> baudRates) {
    timer = new Timer();

    connect(timer, SIGNAL(finished()), timer, SLOT(deleteLater()));
    connect(timer, SIGNAL(signTimed()), this, SLOT(slotRepeatMsgs()));

    timer->start();

    fstream file("../AdamScripts/HomePos/home_pos.txt");
    std::stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    if (buffer.str().size() == 0) {
        qDebug() << "Error: Can not find home positons";
        return;
    }
    Document document;
    document.Parse(buffer.str().c_str());
    document.GetObject();

    for (auto& hubV : document["hubs"].GetArray()) {
        string portName = hubV["portName"].GetString();
        map<int, int> servos;
        for (auto& servoV : hubV["servos"].GetArray())
            servos.emplace(servoV["id"].GetInt(), servoV["pos"].GetInt());
        homePositions.emplace(portName, servos);
    }

    init(deviceNames, protocolVersions, baudRates);
    // {
    // fstream file("default.move");
    // std::stringstream buffer;
    // buffer << file.rdbuf();
    // file.close();
    // if (buffer.str().size() == 0) return;
    // QString data = QString::fromStdString(buffer.str());
    // didDefaultServoMsg(data);
    // }

    {
    fstream file("script.move");
    std::stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    if (buffer.str().size() == 0) return;
    QString data = QString::fromStdString(buffer.str());
    didUploadScriptMsg(data);
    }


}

void Hub::init(vector<string> deviceNames, vector<float> protocolVersions, vector<int> baudRates) {
    int size = deviceNames.size();
    portHandlers.resize(size);
    packetHandlers.resize(size);
    groupSyncWrites.resize(size);

    for (int i = 0; i < size; i++) {
        portHandlers[i] = dynamixel::PortHandler::getPortHandler(deviceNames[i].c_str());
        packetHandlers[i] = dynamixel::PacketHandler::getPacketHandler(protocolVersions[i]);
        groupSyncWrites[i] = nullptr;

//        if (portHandlers[i]->openPort())
//            createStatusMsg("Succeeded to open the port");
//        else
//            createStatusMsg("Failed to open the port", StatusMsgType::ERROR);

//        if (portHandlers[i]->setBaudRate(baudRate))
//            createStatusMsg("Succeeded to change the baudrate");
//        else
//            createStatusMsg("Failed to change the baudrate", StatusMsgType::ERROR);

        if (portHandlers[i]->openPort())
            qDebug() << "Succeeded to open the port";
        else
            qDebug() << "Failed to open the port";

        if (portHandlers[i]->setBaudRate(baudRates[i]))
            qDebug() << "Succeeded to change the baudrate";
        else
            qDebug() << "Failed to change the baudrate";
    }

    // dynamixel::PacketHandler *packetHandler1 = dynamixel::PacketHandler::getPacketHandler(1.0);
    // dynamixel::PortHandler *portHandler = dynamixel::PortHandler::getPortHandler("/dev/ttyUSB0");
    // portHandler->setBaudRate(115200);
    // uint8_t id = 242;
    // uint16_t addr = 36;
    // uint16_t len = 2;
    //
    // uint8_t  dxl_error       = 0;
    // int      dxl_comm_result = COMM_TX_FAIL;
    // uint8_t *data            = (uint8_t*)calloc(len, sizeof(uint8_t));
    //
    // dxl_comm_result = packetHandler1->readTxRx(portHandler, id, addr, len, data, &dxl_error);
    // if (dxl_comm_result == COMM_SUCCESS)
    // {
    //   if (dxl_error != 0)
    //     printf("%s\n", packetHandler1->getRxPacketError(dxl_error));
    //
    //   if (id != BROADCAST_ID)
    //   {
    //     fprintf(stderr, "\n");
    //     for (int i = addr; i < addr+len; i++)
    //     fprintf(stderr, "ADDR %.3d [0x%.4X] :     %.3d [0x%.2X] \n", i, i, data[i-addr], data[i-addr]);
    //     fprintf(stderr, "\n");
    //   }
    // }
    // else
    // {
    //   printf("%s\n", packetHandler1->getTxRxResult(dxl_comm_result));
    //   fprintf(stderr, "\n Fail to read! \n\n");
    // }
    //
    // free(data);
}

int Hub::getHomePosition(int id) {
    for (auto& hub : homePositions) {
        if (hub.second.find(id) == hub.second.end())
            continue;
        return hub.second[id];
    }
    return 0;
}

void Hub::createStatusMsg(std::string data, StatusMsgType::Type type) {
    StatusMsg statusMsg(data);
    statusMsg.addMsg("Hub");
    statusMsg.setType(type);
    emit signCreatedStatusMsg(statusMsg);
}

void Hub::initGSW(uint16_t startAddress, uint16_t dataLength) {
    for (int i = 0; i < groupSyncWrites.size(); i++) {
        if (groupSyncWrites[i] != nullptr)
            delete groupSyncWrites[i];
        groupSyncWrites[i] = new dynamixel::GroupSyncWrite(portHandlers[i], packetHandlers[i], startAddress, dataLength);
    }
}

void Hub::addParamGSW(uint8_t id, uint8_t *data) {
    for (auto &groupSyncWrite: groupSyncWrites) {
        bool result = groupSyncWrite->addParam(id, data);
        if (!result)
            createStatusMsg("[ID:" + to_string(id) + " groupSyncWrite addparam failed", StatusMsgType::ERROR);
            //fprintf(stderr, "[ID:%03d] groupSyncWrite addparam failed", id);
    }
}

void Hub::writeGSW() {
    for (int i = 0; i < groupSyncWrites.size(); i++) {
        int result = groupSyncWrites[i]->txPacket();
        //createStatusMsg("write gsw " + to_string(result));
        /*if (result != COMM_SUCCESS)
            //createStatusMsg(packetHandlers[i]->getTxRxResult(result), StatusMsgType::ERROR);
            printf("%s\n", packetHandlers[i]->getTxRxResult(result));
            */
    }
}

void Hub::clearGSW() {
    for (auto &groupSyncWrite: groupSyncWrites) {
        groupSyncWrite->clearParam();
    }
}

int Hub::read(uint8_t id, uint16_t address, uint16_t length, uint8_t *data) {
    std::string msg = "";
    for (int i = 0; i < packetHandlers.size(); i++) {
        uint8_t error = 0;
        int result = packetHandlers[i]->readTxRx(portHandlers[i], id, address, length, data, &error);
        if (result != COMM_SUCCESS)
            msg = packetHandlers[i]->getTxRxResult(result);
        else if (error != 0)
            msg = packetHandlers[i]->getRxPacketError(error);
        else
            return 0;
    }

    printf("Read error id = %d: %s\n", id, msg.c_str());
    return 1;
}

void Hub::didWriteMsg(QString data) {
    rapidjson::Document document;
    try {
        document.Parse(data.toStdString().data());
        document.GetObject();
    }
    catch(...) {
        qDebug() << "Bad json data " << data;
        createStatusMsg("Bad json data", StatusMsgType::ERROR);
        return;
    }

    //qDebug() << data;
    auto firstField = document["fields"].MemberBegin();
    initGSW(firstField->value["addr"].GetInt(), firstField->value["size"].GetInt());

    for (auto &field: document["fields"].GetObject()) {
        Value &value = field.value;
        switch (value["type"].GetInt()) {
        case FieldType::SERVOFIELD: {
            int size = value["size"].GetInt();
            uint8_t *params = new uint8_t[size];
            const Value &data = value["data"].GetArray();
            for (int i = 0; i < size; i++)
                params[i] = data[i].GetInt();

            if (firstField->value["addr"].GetInt() == 30) {
                uint16_t tmp = uint16_t(params[0] | params[1] << 8);
                tmp += getHomePosition(value["id"].GetInt());
                for (int i = 0; i < 2; i++)
                    params[i] = ((unsigned char *)(&tmp))[i];
            }

            // qDebug() << value["id"].GetInt() << " " << params[0];
            //createStatusMsg(to_string(value["id"].GetInt()) + " " + to_string(params[0]));
            addParamGSW(value["id"].GetInt(), params);
            break;
        }
        }
    }

    writeGSW();
    clearGSW();
}

void Hub::slotClearRepeatMsgs(int) {
    repeatMsgs.clear();
}

void Hub::didReadMsg(QString data) {
    QByteArray result = readServoData(data);
    emit signReaded(result);
}

void Hub::didUploadScriptMsg(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    currentScript.clear();
    scriptMaxTime = 0;
    scriptAddr = 0;
    scriptSize = 0;

    for (auto &field: document["fields"].GetObject()) {
        Value &fieldValue = field.value;
        for (auto &unit: fieldValue["units"].GetArray()) {
            int time = unit["Item1"].GetInt();
            scriptMaxTime = max(scriptMaxTime, time);
            for (auto &servo: unit["Item2"].GetArray()) {
                scriptAddr = servo["addr"].GetInt();
                scriptSize = servo["size"].GetInt();

                int id = servo["id"].GetInt();

                int size = servo["size"].GetInt();
                uint8_t *params = new uint8_t[size];
                const Value &data = servo["data"].GetArray();
                for (int i = 0; i < size; i++)
                    params[i] = data[i].GetInt();

                int16_t angle = params[0] | params[1] << 8;
                int16_t speed = params[2] | params[3] << 8;

                currentScript[id].emplace(make_pair(time, make_pair(angle, speed)));
            }
        }
    }

    fstream file("script.move", std::ofstream::out | std::ofstream::trunc);
    file << data.toStdString();
    file.close();

    qDebug() << "upload servo script " << scriptMaxTime;
//    while(true) {};
}

void Hub::didScriptIterate(int time, std::vector<int> ids) {
    initGSW(scriptAddr, scriptSize);
    for (auto &entry: currentScript) {

        bool flag = false;
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == entry.first) {
                //clearGSW();
                //return;
                flag = true;
                break;
            }
        }
        if (!flag)
            continue;

        auto right = entry.second.begin();
        auto left = right;
        while (right != entry.second.end()) {
            if (time <= (*right).first) {
                //setTime(currentTime);
                if (right == left) {
//                        qDebug() << (*right).second.first;
                    int16_t angle = (*right).second.first;
                    angle += getHomePosition(entry.first);
                    addParamGSW(entry.first, createAngleAndSpeedParams(angle, (*right).second.second));
                    break;
                }

                int16_t leftAngle  = (*left).second.first;
                int16_t rightAngle = (*right).second.first;

                int16_t angle = (time - (*left).first) / (double)((*right).first - (*left).first) *
                        (rightAngle - leftAngle) + leftAngle;
                //qDebug() << leftAngle << " " << rightAngle << " " << angle;


                angle += getHomePosition(entry.first);

                addParamGSW(entry.first, createAngleAndSpeedParams(angle, (*right).second.second));
                break;
            }
            right++;
            left = prev(right, 1);
        }
    }
    writeGSW();
    clearGSW();
}

void Hub::didDefaultIterate(int time, std::vector<int> ids) {
    int endTime = 200;
    initGSW(scriptAddr, scriptSize);
    for (auto &entry: defaultState) {
        if (currentScript.find(entry.first) == currentScript.end()) continue;

        bool flag = false;
        for (int i = 0; i < ids.size(); i++) {
            if (ids[i] == entry.first) {
                //clearGSW();
                //return;
                flag = true;
                break;
            }
        }
        if (!flag)
            continue;

        auto lastValue = (currentScript.at(entry.first)).end();
        lastValue--;
        int16_t leftAngle  = (*lastValue).second.first;
        int16_t rightAngle = entry.second.first;

        int16_t angle = time / (double)(endTime) * (rightAngle - leftAngle) + leftAngle;

        angle += getHomePosition(entry.first);

        addParamGSW(entry.first, createAngleAndSpeedParams(angle, entry.second.second));
    }
    writeGSW();
    clearGSW();
}

void Hub::didRunScriptMsg(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();
//std::map<int, std::set<std::pair<int, std::pair<int, int>>>> currentScript;
    if (currentScript.size() == 0) return;
    //(time, (angle, speed)))
    qDebug() << "run servo script";
    int currentTime = 0;
    while (currentTime != scriptMaxTime) {
        currentTime++;
        didScriptIterate(currentTime, allIds);
        usleep(10000);
    }
    qDebug() << "end servo script";

    if (defaultState.size() == 0) return;

    qDebug() << "start return to default servo";

    int endTime = 200;
    for (int time = 0; time <= endTime; time++) {
        didDefaultIterate(time, allIds);
        usleep(10000);
    }

    qDebug() << "end return to default servo";
}

uint8_t* Hub::createAngleAndSpeedParams(int16_t angle, int16_t speed) {
    uint8_t *params = new uint8_t[4];
    for (int i = 0; i < 2; i++) {
        params[i] = (angle >> (8*i)) & 0xff;
        params[i + 2] = (speed >> (8*i)) & 0xff;
    }
    return params;
}

void Hub::didStopRepeatReadMsg(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    int id = document["id"].GetInt();
    repeatMsgs.erase(id);
}

QByteArray Hub::readServoData(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    QByteArray result(0, ' ');

    for (auto &field: document["fields"].GetObject()) {
        Value &value = field.value;
        switch (value["type"].GetInt()) {
        case FieldType::SERVOFIELD: {
            uint8_t id = value["id"].GetInt();
            uint16_t addr = value["addr"].GetInt();
            uint16_t size = value["size"].GetInt();
            uint8_t *params = new uint8_t[size];

            //read(id, addr, size, params);
            //qDebug() << id;
            if (read(id, addr, size, params) == 1) {
                // qDebug() << id;
                return result;
            }

            Value arr(kArrayType);
            Document::AllocatorType& allocator = document.GetAllocator();
            for (int i = 0; i < size; i++)
                arr.PushBack(params[i], allocator);

            value["data"] = arr;

            StringBuffer buffer;
            buffer.Clear();
            Writer<StringBuffer> writer(buffer);
            document.Accept(writer);

            result = QByteArray(4, ' ');

            for (int i = 0; i < 4; i++)
                result[i] = (char)((buffer.GetSize() >> ((3 - i) * 8)) & 0xFF);
            result.append(buffer.GetString());

            break;
        }
        }
    }

    return result;
}

void Hub::slotParseMsg(QString data) {
//    qDebug() << data.size();
//    qDebug() << data;
//    while (true) {}
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    if (document.IsObject()) {
        document.GetObject();
    }
    else {
        qDebug() << "Bad json data " << data;
        return;
    }

    switch (document["command"].GetInt()) {
    case Command::WRITE: {
        didWriteMsg(data);
        break;
    }
    case Command::REPEATREAD: {
        int id = document["id"].GetInt();
        repeatMsgs.emplace(id, data);
//        readMsgs.push_back(data);
        break;
    }
    case Command::READ: {
        didReadMsg(data);
        break;
    }
    case Command::STOPREAD: {
        didStopRepeatReadMsg(data);
        break;
    }
    case Command::UPLOADSERVOSCRIPT: {
        didUploadScriptMsg(data);
        break;
    }
    case Command::RUNSERVOSCRIPT: {
//        qDebug() << "run";
        didRunScriptMsg(data);
        break;
    }
    case Command::DEFAULTSERVO: {
        didDefaultServoMsg(data);
        break;
    }
    }
}

void Hub::didDefaultServoMsg(QString data) {
    rapidjson::Document document;
    document.Parse(data.toStdString().data());
    document.GetObject();

    defaultState.clear();

    for (auto &field: document["fields"].GetObject()) {
        Value &fieldValue = field.value;
        for (auto &unit: fieldValue["units"].GetArray()) {
            for (auto &servo: unit.GetArray()) {
                int id = servo["id"].GetInt();
                int size = servo["size"].GetInt();
                uint8_t *params = new uint8_t[size];
                const Value &data = servo["data"].GetArray();
                for (int i = 0; i < size; i++)
                    params[i] = data[i].GetInt();

                int16_t angle = params[0] | params[1] << 8;
                int16_t speed = params[2] | params[3] << 8;

                defaultState[id] = make_pair(angle, speed);
            }
        }
    }

    fstream file("default.move", std::ofstream::out | std::ofstream::trunc);
    file << data.toStdString();
    file.close();
    qDebug() << "upload default servos " << defaultState.size();
}

void Hub::slotRepeatMsgs() {
    QByteArray result(0, ' ');
    for (auto &msg: repeatMsgs) {
        result.append(readServoData(msg.second));
    }

    if (result.size() > 0)
        emit signReaded(result);
}

HubController::HubController(std::vector<std::string> deviceNames, std::vector<float> protocolVersions, std::vector<int> baudRates) {
    this->deviceNames = deviceNames;
    this->protocolVersions = protocolVersions;
    this->baudRates = baudRates;
}

void HubController::run() {
    hub = new Hub();
//    exec();
}

void HubController::init() {
    hub->init(deviceNames, protocolVersions, baudRates);
}
