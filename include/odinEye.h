#pragma once

#define SLAVE_ADDRESS 0xBE //real address will be 5F
#define RGBLED_RED 2
#define RGBLED_BLUE 4
#define RGBLED_GREEN 3
#define RB_COLOR 0x21
#define BG_COLOR 0x23
#define RG_COLOR 0x31
#define RGB_COLOR 0x33

#define CLR_SCR_COMMAND 0xFF
#define OFF_LED_CLOCKWISE 0xFE
#define OFF_LED_ANTICLOCKWISE 0xFD
#define ON_LED_CLOCKWISE 0xFC
#define ON_LED_ANTICLOCKWISE 0xFB
#define PLAY_ANIMATION 0xFA
#define SET_ADDRESS 0x0A


#define COMMAND_BYTE 0
#define COLOR_BYTE 1
#define START_LED_BYTE 2
#define FINISH_LED_BYTE 3
#define STEP_LED_BYTE 4
#define DELAY_LED_BYTE 5
#define DELAY_FRAME_BYTE 6

#define SPIRAL_ANIM 0x01
#define FAN_ANIM 0x02

extern "C" {
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <inttypes.h>
    #include <errno.h>
}

namespace RGBLed {
    uint8_t *clrScreen();
    uint8_t *onLedClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
    uint8_t *onLedAntiClockwise(uint8_t color, uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
    uint8_t *offLedClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
    uint8_t *offLedAntiClockwise(uint8_t startLED, uint8_t finishLED, uint8_t stepLED, uint8_t delayLED, uint8_t delayFrame);
    uint8_t *playAnim(uint8_t color, uint8_t anim, uint8_t repCount, uint8_t delayLED, uint8_t delayFrame);
    uint8_t *setAddress(uint8_t address);
    uint8_t get_color(uint8_t color);
}
