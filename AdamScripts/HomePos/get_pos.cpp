#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

#include "dynamixel_sdk.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

struct hub {
    string portName;
    int baudRate;

    bool operator==(const hub &o) const {
        return portName == o.portName && baudRate == o.baudRate;
    }

    bool operator<(const hub &o)  const {
        return portName < o.portName || (portName == o.portName && baudRate < o.baudRate);
    }
};

enum servoID {
    BodyRotate      = 242,
    BodyTop         = 241,
    BodyBot         = 240,
    LeftLeg1        = 236,
    LeftLeg2        = 235,
    LeftLeg3        = 234,
    RightLeg1       = 239,
    RightLeg2       = 238,
    RightLeg3       = 237,
    LeftHandRotate  = 245,
    LeftHand1       = 246,
    LeftHand2       = 244,
    LeftHand3       = 243,
    RightHandRotate = 249,
    RightHand1      = 250,
    RightHand2      = 248,
    RightHand3      = 247,
    HeadRotate      = 2,
    HeadNod         = 1
};

map<hub, vector<servoID>> hubs = {
    {
        { "/dev/ttyUSB1", 115200 },
        {
            servoID::BodyRotate,
            servoID::BodyTop,
            servoID::BodyBot,
            servoID::LeftLeg1,
            servoID::LeftLeg2,
            servoID::LeftLeg3,
            servoID::RightLeg1,
            servoID::RightLeg2,
            servoID::RightLeg3,
            servoID::LeftHandRotate,
            servoID::LeftHand1,
            servoID::LeftHand2,
            servoID::LeftHand3,
            servoID::RightHandRotate,
            servoID::RightHand1,
            servoID::RightHand2,
            servoID::RightHand3
        }
    },
    {
        { "/dev/ttyUSB0", 1000000 },
        {
            servoID::HeadRotate,
            servoID::HeadNod
        }
     }
};

int main() {
    Document document;
    Document::AllocatorType& allocator = document.GetAllocator();
    document.SetObject();

    Value hubsV(kArrayType);

    for (auto& hub : hubs) {
        Value hubV(kObjectType);

        Value portNameV;
        portNameV.SetString(hub.first.portName.c_str(), hub.first.portName.length(), allocator);
        hubV.AddMember("portName", portNameV, allocator);

        dynamixel::PortHandler *portHandler = dynamixel::PortHandler::getPortHandler(hub.first.portName.c_str());
        dynamixel::PacketHandler *packetHandler = dynamixel::PacketHandler::getPacketHandler(1.0);

        if (portHandler->openPort()) {
            printf("Succeeded to open the port!\n");
        }
        else {
            printf("Failed to open the port!\n");
            continue;
        }

        if (portHandler->setBaudRate(hub.first.baudRate)) {
            printf("Succeeded to change the baudrate!\n");
        }
        else {
            printf("Failed to change the baudrate!\n");
            continue;
        }

        Value servosV(kArrayType);

        int dxl_comm_result = COMM_TX_FAIL;
        uint8_t dxl_error = 0;
        uint16_t dxl_present_position = 0;

        for (auto& id : hub.second) {
            dxl_comm_result = packetHandler->read2ByteTxRx(portHandler, id, 36, &dxl_present_position, &dxl_error);
            if (dxl_comm_result != COMM_SUCCESS) {
                printf("%s\n", packetHandler->getTxRxResult(dxl_comm_result));
            }
            else if (dxl_error != 0) {
                printf("%s\n", packetHandler->getRxPacketError(dxl_error));
            }

            printf("[ID:%03d] PresPos:%03d\n", id, dxl_present_position);

            Value servoV(kObjectType);
            servoV.AddMember("id", id, allocator);
            servoV.AddMember("pos", dxl_present_position, allocator);

            servosV.PushBack(servoV, allocator);
        }

        hubV.AddMember("servos", servosV, allocator);
        hubsV.PushBack(hubV, allocator);

        portHandler->closePort();
    }

    document.AddMember("hubs", hubsV, allocator);

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    document.Accept(writer);

    fstream file("home_pos.txt", std::ofstream::out | std::ofstream::trunc);
    file << buffer.GetString();
    file.close();

    return 0;
}
