#include <iostream>
#include <map>

#include "odin_eye.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>

using namespace std;

namespace LedAddress {
enum Type {
    LEFT  = 0
};
}

static std::map<LedAddress::Type, int> ledAddress = {
        { LedAddress::LEFT,  0x5f }
};

int i2c;

int main() {
    LedAddress::Type address = LedAddress::LEFT;
    if ((i2c = open("/dev/i2c-1", O_RDWR)) < 0)
       cout << "Can not open i2c " << i2c << endl;
    else
       cout << "Open i2c " << i2c << endl;

    int ioc;
    if ((ioc = ioctl(i2c, I2C_SLAVE, ledAddress.at(address))) < 0)
        cout << "Can not set i2c device address " << address << endl;
    else
        cout << "Open i2c device address " << address << endl;

    int color = 0;
    while (true) {
        color = (color + 1) % 3;
        cout << color + 2 << endl;
        uint8_t* msg = onLedNow(color + 2, 1, 80, 1, 0, 0);
        write(i2c, msg, 8);
        uint8_t answer = -1;
        while (answer != 0) read(i2c, &answer, 1);
        usleep(5000000);
        msg = clrScreen();
        write(i2c, msg, 8);
        answer = -1;
        while (answer != 0) read(i2c, &answer, 1);
    }

    return 0;
}
