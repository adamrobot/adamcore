#pragma once
#include <vector>
#include <map>
#include <QObject>
#include <QThread>
//#include <QDebug>
#include <QTcpSocket>
#include <QTcpServer>
#include <QUdpSocket>
#include <QThreadPool>
#include <QAudioOutput>
#include <QBuffer>
#include <QByteArray>

#include "server.h"
#include "timer.h"
#include "hub.h"
#include "camera.h"
#include "statusLog.h"
#include "led.h"

class Core: public QObject {
    Q_OBJECT
public:
    Core(QObject *parent = 0);
private:
    StatusLog *statusLog;

    Server *robotServer;
    Server *ledServer;
    Server *wheelServer;
    QUdpSocket* udpSocket;

    LedController *ledController;
    HubController *hubController;
//    WheelController *wheelController;
    void createStatusMsg(std::string);
public slots:
    void slotRobotReaded(QByteArray);
    void slotSendVideo(qintptr);
    void slotUdpRead();
signals:
    void signUdpReaded(QString);
    void signChangeCmd(QString);
    void signReadCmd(QString);
    void signCreatedStatusMsg(StatusMsg);
};
