#include "timer.h"

void Timer::run() {
    while (true) {
        ticks++;
        if (ticks == 100) {
            emit signTimed();
            ticks = 0;
        }
        usleep(1000);
    }
    exec();
}
