#pragma once
#include <QObject>
#include <QThread>
//#include <QDebug>
#include <QTcpSocket>
#include <QTcpServer>
#include <QThreadPool>
#include <map>
#include <string>
#include <thread>

#include "client.h"
//#include "timer.h"
//#include "message.h"
//#include "messagecontroller.h"

class Server: public QTcpServer {
    Q_OBJECT
public:
    Server(int port, QObject *parent = 0);
    void start();
    void write(std::string data);
    void write(QByteArray data);
    void write(std::string data, qintptr descriptor);
    void clear();
    std::map<qintptr, Client*> clients;
private:
    float port;
    std::thread audio;
    void createStatusMsg(std::string, StatusMsgType::Type);
    void createStatusMsg(StatusMsg, StatusMsgType::Type);
protected:
    void incomingConnection(qintptr handle) override;
signals:
    void signCreatedStatusMsg(StatusMsg);
    void signClientDeleted(int);
    void signNewConnection(qintptr);
    void signReaded(QString);
public slots:
    void slotCreateStatusMsg(StatusMsg);
    void slotReaded(QString);
    void slotEraseClient(int);
};
