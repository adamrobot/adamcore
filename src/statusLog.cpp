#include "statusLog.h"

StatusLog::StatusLog(int port) {
    this->port = port;
    server = new Server(port);

    connect(server, SIGNAL(signCreatedStatusMsg(StatusMsg)), this, SLOT(slotPrintMsg(StatusMsg)));

//    server->start();
}

void StatusLog::slotPrintMsg(StatusMsg msg) {
    qDebug() << msg.getResultMsg().c_str();
//    server->write(msg.getData());
}
