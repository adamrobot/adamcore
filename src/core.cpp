#include <QObject>
#include <QCoreApplication>
#include <QFile>
#include <iostream>
#include <vector>

//#include "opencv2/videoio.hpp"
//#include "opencv2/highgui.hpp"
//#include "opencv2/imgproc.hpp"

//#include "unistd.h"

#include "core.h"

//#include <rapidjson/filereadstream.h>

//#define DEVICENAME                      "/dev/cu.wchusbserial1420"
//#define DEVICENAME                      "/dev/ttyUSB0"
//#define BAUDRATE                        57600
//#define PROTOCOL_VERSION                1.0

using namespace std;
using namespace rapidjson;

Core::Core(QObject *parent) : QObject(parent) {
    statusLog  = new StatusLog(4000);

    robotServer = new Server(5000);
    ledServer   = new Server(6100);
    //audioServer = new Server(7000);
//    audioServer = new Server(8079);
//    videoServer = new Server(5000);

//    timer = new Timer();

    QThread *hubThread= new QThread;
    // Hub *hub = new Hub({ PORT1, PORT2 }, 1.0, 57600);
    Hub *hub = new Hub({ PORT1, PORT2 }, { 1.0, 1.0 }, { 115200, 1000000 });

    hub->moveToThread(hubThread);

    connect(hub, SIGNAL(signCreatedStatusMsg(StatusMsg)), statusLog, SLOT(slotPrintMsg(StatusMsg)));
    connect(hubThread, SIGNAL(finished()), hubThread, SLOT(deleteLater()));
    connect(robotServer, SIGNAL(signNewConnection(qintptr)), this, SLOT(slotSendVideo(qintptr)));
    connect(robotServer, SIGNAL(signCreatedStatusMsg(StatusMsg)), statusLog, SLOT(slotPrintMsg(StatusMsg)));
    connect(robotServer, SIGNAL(signClientDeleted(int)), hub, SLOT(slotClearRepeatMsgs(int)));
    connect(robotServer, SIGNAL(signReaded(QString)), hub, SLOT(slotParseMsg(QString)));
    connect(hub, SIGNAL(signReaded(QByteArray)), this, SLOT(slotRobotReaded(QByteArray)));

    //------------------udp socket

    udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::Any, 6000);
    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(slotUdpRead()));
    connect(this, SIGNAL(signUdpReaded(QString)), hub, SLOT(slotParseMsg(QString)));

    //------------------udp socket

    QThread *ledThread= new QThread;
    LedController *lc = new LedController();

    lc->moveToThread(ledThread);

    connect(ledThread, SIGNAL(finished()), ledThread, SLOT(deleteLater()));
    connect(ledServer, SIGNAL(signCreatedStatusMsg(StatusMsg)), statusLog, SLOT(slotPrintMsg(StatusMsg)));
    connect(ledServer, SIGNAL(signReaded(QString)), lc, SLOT(slotParseMsg(QString)));

    robotServer->start();
    ledServer->start();

    hubThread->start();
    ledThread->start();
}

void Core::slotUdpRead() {
    QByteArray datagram;
    datagram.resize(udpSocket->pendingDatagramSize());
    QHostAddress *address = new QHostAddress();
    udpSocket->readDatagram(datagram.data(), datagram.size(), address);

    // qDebug() << "Size: " << datagram.size();
    // qDebug() << "Data: " << QString::fromStdString(datagram.mid(4, -1).toStdString());
    emit signUdpReaded(QString::fromStdString(datagram.mid(4, -1).toStdString()));
}

void Core::createStatusMsg(std::string data) {
    StatusMsg statusMsg(data);
    statusMsg.addMsg("Core");
    emit signCreatedStatusMsg(statusMsg);
}

void Core::slotSendVideo(qintptr handle) {
    qDebug() << robotServer->clients.at(handle)->peerAddress().toString();
}

void Core::slotRobotReaded(QByteArray data) {
    robotServer->write(data.toStdString());
}
