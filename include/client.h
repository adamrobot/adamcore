#pragma once
#include <QObject>
#include <QTcpSocket>
#include <QThread>
#include <string>
#include <memory>
#include <QHostAddress>

#include "statusMsg.h"

class Client: public QObject {
    Q_OBJECT
public:
    Client(qintptr descriptor, QObject *parent = 0);
    void init();
    void clear();
//    void run();
    qint64 write(std::string data);
    qint64 write(QByteArray data);
    QHostAddress peerAddress();
//    void setDescriptor(qintptr descriptor);
//    void connect(const QString &hostName, float port);
private:
    QTcpSocket *socket;
    quint32 blockSize;
    QByteArray currentBlock;
    QByteArray currentData;
    int descriptor;
    void createStatusMsg(std::string, StatusMsgType::Type);
signals:
    void signDeleted(int);
    void signReaded(QString);
    void signCreatedStatusMsg(StatusMsg);
public slots:
//    void slotWrite(std::string data);
    void slotConnected();
    void slotDisconnected();
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);
};
typedef std::shared_ptr<Client> PClient;
