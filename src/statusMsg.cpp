#include "statusMsg.h"

using namespace StatusMsgType;
using namespace std;
using namespace rapidjson;

StatusMsg::StatusMsg() : type(INFO) {
}

StatusMsg::StatusMsg(std::string msg) : type(INFO) {
    msgs.push_back(msg);
}

std::string StatusMsg::getResultMsg() {
    std::string result = "";
    for (int i = msgs.size() - 1; i >= 0; i--)
        result += msgs[i] + ". ";
    return result;
}

void StatusMsg::addMsg(const std::string msg) {
    msgs.push_back(msg);
}

void StatusMsg::setType(const StatusMsgType::Type type) {
    if (this->type != ERROR)
        this->type = type;
}

std::string StatusMsg::getData() {
    rapidjson::Document *document = new Document(kObjectType);

    document->SetObject();
    Document::AllocatorType& alloc = document->GetAllocator();
    // Value str(kStringType);
    // str.SetString(getResultMsg().data(), alloc);
    // document->AddMember("message", str, alloc);
    document->AddMember("type", (int)type, alloc);
    Value msgsJson(kArrayType);
    for (auto msg: msgs) {
        Value msgJson(kStringType);
        msgJson.SetString(msg.data(), alloc);
        msgsJson.PushBack(msgJson, alloc);
    }
    document->AddMember("messages", msgsJson, alloc);

    StringBuffer buffer;
    buffer.Clear();
    Writer<StringBuffer> writer(buffer);
    document->Accept(writer);
    std::string result(4, 0);
    for (int i = 0; i < 4; i++)
        result[i] = (char)((buffer.GetSize() >> ((3 - i) * 8)) & 0xFF);

    result.append(buffer.GetString());

    return result;
}
