import sys

import board
import busio
import time
import threading
import adafruit_pca9685
from adafruit_motor import motor

i2c = busio.I2C(board.SCL, board.SDA)

pca = adafruit_pca9685.PCA9685(i2c, address=0x40)
pca.frequency = 1000

wheels_id = [0]
speed = 0.0
if __name__ == "__main__":
    if len (sys.argv) > 1:
        speed = float(sys.argv[1])
        wheels_id = list(map(int, sys.argv[2:]))

wheels = [[2, 3], [1, 0], [4, 5], [7, 6]]

def set_velocity(ch1, ch2, value):
    channel1 = pca.channels[ch1]
    channel2 = pca.channels[ch2]
    motor1 = motor.DCMotor(channel1, channel2)
    motor1.throttle = value

for wheel_id in wheels_id:
    set_velocity(wheels[wheel_id][0], wheels[wheel_id][1], speed)
