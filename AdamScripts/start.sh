#!/bin/bash

trap "exit" INT TERM ERR
trap "kill 0" EXIT

python3 WheelsServer.py &
python3 Fingers/server.py &
./../build-dir/AdmCore &

wait
